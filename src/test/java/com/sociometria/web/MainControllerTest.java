package com.sociometria.web;

/**
 * Created by ayurtaev on 23.08.16.
 */

import com.sociometria.web.controllers.rest.HelloRestControllerTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MainControllerTest extends HelloRestControllerTest {

    @Test
    public void getHello() throws Exception {
        getMockMvc().perform(MockMvcRequestBuilders.get("/").accept(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("")));
    }
}