package com.sociometria.web.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "persistent_logins")
@AllArgsConstructor
@NoArgsConstructor
public class PersistentLogin {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "series",
            unique = true, nullable = false)
    private String series;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    private User user;

    @Column(name = "token", nullable = false, length = 64)
    private String token;

    @Column(name = "last_used", nullable = false, length = 64)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUsed;
}