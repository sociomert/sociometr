package com.sociometria.web.controllers.rest;

import com.sociometria.web.model.Employee;
import com.sociometria.web.model.Employees;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 *
 * Created by ayurtaev on 26.08.16.
 */
@RestController
public class HelloRestController {

    @RequestMapping("/api/employees")
    public Employees greeting() {
        return new Employees(Collections.singletonList(new Employee("Alex", "Y", "dsc")));
    }
}
