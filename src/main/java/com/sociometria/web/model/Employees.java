package com.sociometria.web.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 *
 * Created by ayurtaev on 26.08.16.
 */
@Getter
@Setter
@AllArgsConstructor
public class Employees {
    private List<Employee> employees;
}
