package com.sociometria.web.model;

import lombok.Data;

/**
 * Created by ayurtaev on 26.08.16.
 */
@Data
public class Employee {

    private Long id;
    private String firstName;
    private String lastName;
    private String description;

    private Employee() {}

    public Employee(String firstName, String lastName, String description) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
    }

}
