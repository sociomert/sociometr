package com.sociometria.web.repository;

import com.sociometria.web.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * Created by ayurtaev on 24.08.16.
 */
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByEmail(String username);
}
