package com.sociometria.web.repository;

import com.sociometria.web.entity.PersistentLogin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * This repository doesn't exported by Spring Data REST
 *
 * Created by ayurtaev on 27.08.16.
 */
@RepositoryRestResource(exported = false)
public interface PersistentLoginRepository extends CrudRepository<PersistentLogin, String> {

    /**
     * Usage example:
     * @PreAuthorize("@persistentLoginRepository.findOne(#id)?.user?.email == authentication?.name")
     *
     * @param persistentLogin
     * @return
     */
    @Override
//    @PreAuthorize("#item?.user?.email == authentication?.name")
    PersistentLogin save(@Param("item") PersistentLogin persistentLogin);
}
