package com.sociometria.web;

import com.sociometria.web.config.ApplicationConfig;
import com.sociometria.web.config.JpaConfig;
import com.sociometria.web.config.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

/**
 *
 * Created by ayurtaev on 23.08.16.
 */
@Configuration
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static Class<?>[] SOURCES = new Class<?>[]{Application.class, ApplicationConfig.class, SecurityConfig.class,
            JpaConfig.class,
            LiquibaseProperties.class, SecurityConfig.class};

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SOURCES, args);

//        System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//        String[] beanNames = ctx.getBeanDefinitionNames();
//        Arrays.sort(beanNames);
//        for (String beanName : beanNames) {
//            System.out.println(beanName);
//        }
    }

}
